package surebet;
import java.util.*;

/**
 * <h2>Calculadora de SureBets</h2>
 * @version 1-2020
 * @author Santy �lvarez Osuca
 * @since 23-02-2020
 */


public class surebet {
	/**
	 * Objeto Scanner como global
	 */
	static Scanner reader = new Scanner(System.in);
	public static void main(String[] args) {
		/**
		 * Numero de casos que tendra la prueba
		 */
		int casos = reader.nextInt();
		while(casos > 0) {
			/**
			 * Primera "cuota" a introducir
			 */
			float x = reader.nextFloat();
			/**
			 * Segunda "cuota" a introducir
			 */
			float y = reader.nextFloat();
			/**
			 * Llamamos a la funcion
			 */
			System.out.println(sureBet(x ,y));
			/**
			 * Restamos los casos
			 */
			casos--;
		}
		reader.close();
	}
	
	/**
	 * 
	 * @param d Primera cuota
	 * @param e Segunda cuota
	 * @return<ul>
	 *           <li>SI: Es una surebet</li>
	 *           <li>NO: No es una surebet</li>
	 *         </ul>

	 */
	public static String sureBet(double d, double e){
		/**
		 * Variable para hacer la operacion
		 */
		double res = (10/d) + (10/e);
		/**
		 * Comprobamos la operacion
		 */
		if(res > 10) return "NO";
		else return "SI";
	}
}